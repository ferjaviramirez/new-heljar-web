// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyDRQEOaNJiW_TA4ZjLggH9yzCHgxpYS3W4",
    authDomain: "static-form-heljar.firebaseapp.com",
    databaseURL:
        "https://static-form-heljar-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "static-form-heljar",
    storageBucket: "static-form-heljar.appspot.com",
    messagingSenderId: "1077246198500",
    appId: "1:1077246198500:web:1e5550671e974a0023e2f1",
    measurementId: "G-HVEF0FGVME",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


// Reference messages collection
var messagesRef = firebase.database().ref('messages');

// Listen for form submit
document.getElementById('contact').addEventListener('submit', submitForm);

// Submit form
function submitForm(e) {
    e.preventDefault();
    //Get value
    var name = getInputVal('name');
    var email = getInputVal('email');
    var message = getInputVal('message');
    var phone = getInputVal('phone');
    // Save message
    saveMessage(name, email, message, phone);
    // Clear form
    document.getElementById('contact').reset();

    sendEmail(name, email, message, phone);
    
}
// Function to get form value
function getInputVal(id) {
    return document.getElementById(id).value;
}

function saveMessage(name, email, message, phone) {
    var newMessageRef = messagesRef.push();
    newMessageRef.set({
        name: name,
        email: email,
        message: message,
        phone: phone
    });
    swal({
        title: "Takk for registreringen hos oss i Norskispania.",
        text: "Følg oss på sosial media for og få spesial tilbud, kampanjer og mere informasjon ved og klikke på linkene under:",
        closeOnClickOutside: true,
        className:["swal-title"],
        buttons: {
            facebook: {
                text: "Facebook",
                value: "facebook"
            },
            instagram: {
                text: "Instagram",
                value: "instagram"
            },
            telegram: {
                text: "LinkedIn",
                value: "linkedin"
            }
        }
    }).then((value) => {
        switch (value) {
            case "facebook":
                window.open("https://www.facebook.com/Norskispania-107462978215049","_blank");
                break;

            case "linkedin":
                window.open("https://www.linkedin.com/company/73220131/admin/",);
                break;

            case "instagram":
                window.open("https://www.instagram.com/norskispania/","_blank");
                break;

        }

    })
        ;
}

//send Email Info

function sendEmail(name, email, message, phone) {
    Email.send({
        Host: "smtp.gmail.com",
        Username: 'ferjaviramirez@gmail.com',
        Password: "fszepfxujjayzzom",
        To: ['ferjaviramirez@gmail.com','post@norskispania.no','kunder@norskispania.no'],
        From: 'ferjaviramirez@gmail.com',
        Subject: `${name} sent you a message`,
        Body: `<h1>Norskispania</h1><br/> 🧑‍🤝‍🧑 Name: ${name} <br/> 📞 Phone: ${phone} <br/> 📧 Email: ${email} <br/> 📬 Message: ${message} <br/>`

    }).then((message) => console.log("Mail has sent successfully"))
        .catch((error) => console.log(error));
}
